@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Crear Categoria
                </div>

                <div class="card-body">
                    <form-component method="POST" action="{{ route('api.admin.categories.store') }}" v-slot="slotProps">
                        <>
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input class="form-control" type="text" name="name" id="name" value="" autofocus />
                            </div>


                            <div class="d-flex justify-content-end">
                                <p>
                                    <a class="btn btn-info" href="{{ route('categories.index') }}">Cancelar</a>
                                    <input class="btn btn-success" type="submit" name="enviar"
                                        value="Guardar cambios" />
                                </p>
                            </div>
                        </>
                    </form-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection