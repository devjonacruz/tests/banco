@extends('layouts.app')

@section('content-header')
<section class="content-header">
    <h1>
        Lista de paquetes
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Paquetes</li>
    </ol>
</section>
@endsection

@section('content')

<div class="row">
    @foreach ($packages as $package)

    <div class="col-md-12 col-lg-4">
        <form-component confirm method="POST"
            action="{{ route('api.orderPackage.store', [$user->username, $package->alias]) }}" v-slot="slotProps">

            <div class="box box-default pull-up">
                <img class="card-img-top img-responsive" src="{{ asset($package->image) }}" alt="Card image cap">
                <div class="box-body text-center">
                    <h4 class="box-title">{{ $package->amount }}</h4>
                    {{-- <p class="box-text">
                            Some quick example text to build on the card title and make up the bulk of the card's
                            content.
                        </p> --}}
                    @if($package->is_active)
                    <button type="button" class="btn btn-info" @click="slotProps.onSubmit">Comprar</button>
                    @else
                    <button type="button" class="btn btn-success">Activo</button>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
        </form-component>
    </div>
    @endforeach
</div>


@endsection

@section('js')
<script>

</script>
@endsection