@extends('layouts.app')

@section('content-header')
<section class="content-header">
    <h1>
        Editar usuario
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Forms</a></li>
        <li class="breadcrumb-item active">Editar usuario</li>
    </ol>
</section>
@endsection

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Información personal</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form-component method="PUT" action="{{ route('api.users.update', $user->username) }}"
                    v-slot="slotProps">

                    <div class="form-group">
                        <h5>Nombres <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}"
                                required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Apellidos <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="last_name" id="last_name" class="form-control"
                                value="{{ $user->last_name }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Usuario <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="username" id="username" class="form-control"
                                value="{{ $user->username }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Email <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}"
                                required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Password</h5>
                        <div class="controls">
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Password Confirmation</h5>
                        <div class="controls">
                            <input type="password" name="password_confirmation" id="password_confirmation"
                                class="form-control">
                        </div>
                    </div>

                    <div class="text-xs-right">
                        <button type="button" class="btn btn-info" @click="slotProps.onSubmit">
                            Actualizar información
                        </button>
                    </div>
                </form-component>
            </div>
        </div>
    </div>

</div>

<div class="box collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">2FA</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form-component method="PUT" action="{{ route('api.usersauthetication.update', $user->username) }}"
                    v-slot="slotProps">

                    @if (!$user->authentication->is_google)
                    <div class="form-group">
                        <div class="controls text-center">
                            <img src="{{ $qr }}" alt="">
                            <copy-text-component inline-template>
                                <p class="pointer" @click="onCopy('{{ $user->authentication->google_secret }}')">
                                    {{ $user->authentication->google_secret }}
                                </p>
                            </copy-text-component>
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        <h5>Code <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="code_2fa" id="code_2fa" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="text-xs-right">
                        @if (!$user->authentication->is_google)
                        <button type="button" class="btn btn-info" @click="slotProps.onSubmit">Crear 2FA</button>
                        @else
                        <button type="button" class="btn btn-warning" @click="slotProps.onSubmit">Eliminar 2FA</button>
                        @endif
                    </div>
                </form-component>
            </div>
        </div>
    </div>
</div>

<div class="box collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Información Financiera</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            @foreach(Auth::user()->wallets as $wallet)
            @php
            if(!$wallet->type_wallet->is_available) continue;
            @endphp
            <div class="col">
                <form-component method="PUT" action="{{ route('api.userswallet.update', $user->username) }}"
                    v-slot="slotProps">

                    <input type="hidden" name="type" id="type" value="{{ $wallet->type_wallet->id }}">
                    <div class="form-group">
                        <h5>Wallet USDT <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="wallet" id="wallet" class="form-control"
                                value="{{ $wallet->wallet }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Code 2FA <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="code_2fa" id="code_2fa" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="text-xs-right">
                        <button type="button" class="btn btn-info" @click="slotProps.onSubmit">Agregar Wallet</button>
                    </div>
                </form-component>
            </div>
            @endforeach
        </div>
    </div>
</div>


<div class="box collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Información Broker</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form-component method="PUT" action="{{ route('api.usersBroker.update', $user->username) }}"
                    v-slot="slotProps">

                    <div class="form-group">
                        <h5>ID <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="broker_id" id="broker_id" class="form-control"
                                value="{{ $user->broker->broker_id }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Servidor <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="broker_server" id="broker_server" class="form-control"
                                value="{{ $user->broker->broker_server }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Contraseña <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="broker_pass" id="broker_pass" class="form-control"
                                value="{{ $user->broker->broker_pass }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Code 2FA <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="code_2fa" id="code_2fa" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="text-xs-right">
                        <button type="button" class="btn btn-info" @click="slotProps.onSubmit">Agregar Broker</button>
                    </div>
                </form-component>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>

</script>
@endsection