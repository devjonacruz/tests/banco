@extends('layouts.app')

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Arbol de usuario</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col">
                <div class="family-tree">
                    <ul>
                        <li>
                            <div>{{ Auth::user()->username }}</div>
                            @if(count(Auth::user()->referrals->filter(function($user){return $user->is_active;})) > 0)
                            <ul>
                                @foreach(Auth::user()->referrals->filter(function($user){return $user->is_active;}) as $referral)
                                <li>
                                    <div>{{ $referral->username }}</div>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')
<script>

</script>
@endsection