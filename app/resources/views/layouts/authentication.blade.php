<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/png" sizes="32x32" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- Template -->

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="/template/html/css/bootstrap-extend.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="/template/html/css/master_style.css" />

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="/template/html/css/skins/_all-skins.css" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>

<body class="hold-transition login-page">
    <div id="app" class="login-box">
        <div class="login-logo">
            <a href="/">
                <img src="{{ asset('img/logo.png') }}" alt="Logo" width="250px" />
            </a>
        </div>

        @yield('content')
    </div>
    @yield('js')
</body>

</html>