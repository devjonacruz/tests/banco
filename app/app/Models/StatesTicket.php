<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatesTicket extends Model
{
    protected $fillable = ['name', 'alias'];
}
