<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAuthentication extends Model
{
    protected $fillable = ['user_id', 'is_google', 'google_secret'];
}
