<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrdersPackagesCommission extends Model
{
    protected $fillable = ['package_id', 'level', 'amount', 'type_pay'];

    /**
     * Get the package that owns the OrdersPackagesCommission
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package(): BelongsTo
    {
        return $this->belongsTo(Package::class);
    }
}
