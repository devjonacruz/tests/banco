<?php

namespace App\Http\Controllers;

use App\Models\StatesTicket;
use App\Models\SupportTicket;
use App\Models\SupportTicketsComment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StatesTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = SupportTicket::where(function ($query) {
            if(!Auth::user()->hasRole('Admin')){
                $query->where('user_id', Auth::user()->id);
            }

        })
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('tickets.index', compact('tickets', 'total', 'responded', 'resolve', 'pending'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'subject' => 'required',
            'comment' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }

        $supportTicket = SupportTicket::create([
            'user_id' => $user->id,
            'subject' => $request->subject,
        ]);

        SupportTicketsComment::create([
            'user_id' => $user->id,
            'support_ticket_id' => $supportTicket->id,
            'comment' => $request->comment,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => "Soporte creado",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function show(SupportTicket $supportTicket)
    {
        return view('tickets.show', compact('supportTicket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, SupportTicket $supportTicket)
    {
        $rules = [
            'comment' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }

        $supportTicket->states_ticket_id = $supportTicket->user_id == $user->id ? 2 : 3;
        $supportTicket->save();

        SupportTicketsComment::create([
            'user_id' => $user->id,
            'support_ticket_id' => $supportTicket->id,
            'comment' => $request->comment,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => "Comentario agregado",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportTicket $supportTicket)
    {
        //
    }
}
