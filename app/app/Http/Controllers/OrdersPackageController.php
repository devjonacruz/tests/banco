<?php

namespace App\Http\Controllers;

use App\Models\OrdersPackage;
use App\Models\Package;
use App\Models\Setting;
use App\Services\Payments\Coinpayments;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Lista de compras';

        $headers = [
            'Paquete',
            'Valor',
            'Estado',
            'Fecha',
        ];

        $body = OrdersPackage::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function ($order) {
                return [
                    $order->package->name,
                    $order->amount,
                    $order->state->name,
                    $order->created_at->diffForHumans(),
                ];
            });

        $datatable = true;

        return view('table', compact('headers', 'body', 'title', 'datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user, Package $package)
    {
        $setting = Setting::where('settings_type_id', 2)->first();

        $orderPending = OrdersPackage::where('user_id', $user->id)->whereIn('state_id', [1, 2])->first();
        if ($orderPending) {
            return response()->json([
                'status' => 'error',
                'message' => 'Tienes transacciones pendientes!',
            ]);
        }

        if (!$package->is_visible) {
            $maxOrder = OrdersPackage::where('user_id', $user->id)
                ->where('state_id', '6')
                ->where('package_id', '<=', $package->id)
                ->orderBy('package_id', 'DESC')
                ->first();

            if ($maxOrder && $maxOrder->package_id < $package->id) {
                $package->amount = $package->amount - $maxOrder->package->amount;
            }
        }

        if ($setting->log > 0) {
            // $package->amount += $package->amount * $setting->log / 100;
            $package->amount += $setting->log;
        }

        $client = new Coinpayments();
        $client->createCustomTransaction($package->amount, 'USD', 'USDT', $user->email);

        $html = [
            'qrcode_url' => "https://api.qrserver.com/v1/create-qr-code/?data={$client->response->address}&amp;size=200x200",
            'amount' => $client->response->amount,
            'address' => $client->response->address,
            'message' => 'En cuanto tengamos 6 confirmaciones, su compra se verá efectiva en el sistema',
        ];

        OrdersPackage::create([
            'user_id' => $user->id,
            'package_id' => $package->id,
            'state_id' => 2,
            'amount' => $package->amount,
            'log' => json_encode($client->response),
            'invoice' => $client->response->txn_id,
        ]);

        return response()->json([
            'status' => 'success',
            'html' => [
                'type' => 'modal',
                'element' => '#modalGenericCenterDefault',
                'body' => view('package.pay', $html)->toHtml(),
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function show(OrdersPackage $ordersPackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdersPackage $ordersPackage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdersPackage $ordersPackage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrdersPackage $ordersPackage)
    {
        //
    }
}
