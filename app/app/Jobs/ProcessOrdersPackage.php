<?php

namespace App\Jobs;

use App\Models\Commission;
use App\Models\OrdersPackage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ProcessOrdersPackage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrdersPackage $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->order->state_id == 3) {
            $user = $this->order->user;
            $user->is_active = true;
            $user->active_start = Carbon::now();
            $user->active_end = Carbon::now()->addDays($this->order->package->days);
            $user->save();

            $commissions = collect($this->order->package->orders_packages_commission);
            $parent = $this->order->user->parent;

            $commissions->each(function ($commision) use (&$parent) {
                if ($parent) {
                    // if ($parent->is_active) {
                    $amount = $commision->type_pay == 'percentage' ? $this->order->amount * $commision->amount / 100 : $commision->amount;

                    $create = Commission::create([
                        'user_id' => $parent->id,
                        'user_grant_id' => $this->order->user_id,
                        'amount' => $amount,
                    ]);

                    if ($create) {
                        collect($parent->wallets)->each(function ($wallet) use ($amount) {
                            $wallet->amount += $amount;
                            $wallet->save();
                        });
                    }
                    // }

                    $parent = $parent->parent;
                }
            });

            $this->order->state_id = 6;
            $this->order->save();
        }
    }
}
