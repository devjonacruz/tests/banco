<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ValidateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::where('active_end', '<', now())
            ->where('is_active', true)
            ->each(function ($user) {
                $user->is_active = false;
                $user->save();
            });
    }
}
